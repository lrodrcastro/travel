from django import forms

class SearchForm(forms.Form):
    origin = forms.CharField(label='Origin', max_length=3)
    destination = forms.CharField(label='Destination', max_length=3)
    departure = forms.DateField(label='Departure', widget=forms.widgets.DateInput(attrs={'type': 'date'}))
    arrival = forms.DateField(label='Arrival', widget=forms.widgets.DateInput(attrs={'type': 'date'}))
    num_adults = forms.IntegerField(label='Adults', min_value=1, max_value=6)
    num_kids = forms.IntegerField(label='Kids', min_value=0, max_value = 6, initial=0)