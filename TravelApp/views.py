from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import SearchForm
import requests

# Create your views here.
def home(request):
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            request.session['origin'] = form.cleaned_data['origin']
            
            #origin = form.cleaned_data['origin']
            #destination = form.cleaned_data['destination']
            #departure = form.cleaned_data['departure']
            #arrival = form.cleaned_data['arrival']
            #num_adults = form.cleaned_data['num_adults']
            #num_kids = form.cleaned_data['num_kids']
            
            #url = "http://developer.goibibo.com/api/search/?app_id=5e3d9003&app_key=ff2ba27058cff4fab374f6ca832eb79a&source={}&destination={}&dateofdeparture={}&dateofarrival={}&seatingclass=E&adults={}&children={}&infants=0&counter=100"
            #r = requests.get(url.format(origin, destination, departure, arrival, num_adults, num_kids)).json()
            
            return HttpResponseRedirect('/pricing')
        
    else:
        form = SearchForm()
            
    context = {}
    return render(request, 'index.html', {'form': form})

def pricing(request):
    text = request.session['origin']
    context = {'text': text}
    return render(request, 'pricing.html', context)

def destination(request):
    return render(request, 'destination.html')

def contact(request):
    return render(request, 'contact.html')